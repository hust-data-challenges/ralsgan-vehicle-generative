import functools

import torch.nn as nn

import modules


# Architectures for G
# Attention is passed in in the format '32_64' to mean applying an attention
# block at both resolution 32x32 and 64x64. Just '64' will apply at 64x64.
def G_arch(ch=64, attention='64', ksize='333333', dilation='111111'):
    arch = {}
    arch['512'] = {'in_channels': [ch * item for item in [16, 16, 8, 8, 4, 2, 1]],
                   'out_channels': [ch * item for item in [16, 8, 8, 4, 2, 1, 1]],
                   'upsample': [True] * 7,
                   'resolution': [8, 16, 32, 64, 128, 256, 512],
                   'attention': {2 ** i: (2 ** i in [int(item) for item in attention.split('_')])
                                 for i in range(3, 10)}}
    arch['256'] = {'in_channels': [ch * item for item in [16, 16, 8, 8, 4, 2]],
                   'out_channels': [ch * item for item in [16, 8, 8, 4, 2, 1]],
                   'upsample': [True] * 6,
                   'resolution': [8, 16, 32, 64, 128, 256],
                   'attention': {2 ** i: (2 ** i in [int(item) for item in attention.split('_')])
                                 for i in range(3, 9)}}
    arch['128'] = {'in_channels': [ch * item for item in [16, 16, 8, 4, 2]],
                   'out_channels': [ch * item for item in [16, 8, 4, 2, 1]],
                   'upsample': [True] * 5,
                   'resolution': [8, 16, 32, 64, 128],
                   'attention': {2 ** i: (2 ** i in [int(item) for item in attention.split('_')])
                                 for i in range(3, 8)}}
    arch['128x'] = {'in_channels': [ch * item for item in [16, 16, 8, 4]],
                    'out_channels': [ch * item for item in [16, 8, 4, 2]],
                    'upsample': [True] * 4,
                    'resolution': [8, 16, 32, 64],
                    'attention': {2 ** i: (2 ** i in [int(item) for item in attention.split('_')])
                                  for i in range(3, 7)}}
    arch['96a'] = {'in_channels': [ch * item for item in [16, 16, 8, 4, 2]],
                   'out_channels': [ch * item for item in [16, 8, 4, 2, 1]],
                   'upsample': [True] * 5,
                   'resolution': [6, 12, 24, 48, 96],
                   'attention': {6 * 2 ** i: (6 * 2 ** i in [int(item) for item in attention.split('_')])
                                 for i in range(0, 5)}}

    arch['96'] = {'in_channels': [ch * item for item in [16, 16, 8, 4]],
                  'out_channels': [ch * item for item in [16, 8, 4, 2]],
                  'upsample': [True] * 4,
                  'resolution': [12, 24, 48, 96],
                  'attention': {12 * 2 ** i: (6 * 2 ** i in [int(item) for item in attention.split('_')]) for i in
                                range(0, 4)}}

    arch['80'] = {'in_channels': [ch * item for item in [16, 16, 8, 4]],
                  'out_channels': [ch * item for item in [16, 8, 4, 2]],
                  'upsample': [True] * 4,
                  'resolution': [10, 20, 40, 80],
                  'attention': {10 * 2 ** i: (2 ** i in [int(item) for item in attention.split('_')]) for i in
                                range(0, 4)}}

    arch['64'] = {'in_channels': [ch * item for item in [16, 16, 8, 4]],
                  'out_channels': [ch * item for item in [16, 8, 4, 2]],
                  'upsample': [True] * 4,
                  'resolution': [8, 16, 32, 64],
                  'attention': {2 ** i: (2 ** i in [int(item) for item in attention.split('_')])
                                for i in range(3, 7)}}
    # arch['32']  = {'in_channels' :  [ch * item for item in [4, 4, 4]],
    #              'out_channels' : [ch * item for item in [4, 4, 4]],
    #              'upsample' : [True] * 3,
    #              'resolution' : [8, 16, 32],
    #              'attention' : {2**i: (2**i in [int(item) for item in attention.split('_')])
    #                             for i in range(3,6)}}
    arch['32'] = {'in_channels': [ch * item for item in [16, 8, 4]],
                  'out_channels': [ch * item for item in [8, 4, 2]],
                  'upsample': [True] * 3,
                  'resolution': [8, 16, 32],
                  'attention': {2 ** i: (2 ** i in [int(item) for item in attention.split('_')])
                                for i in range(3, 6)}}
    return arch


class Generator(nn.Module):
    def __init__(self, config):
        super(Generator, self).__init__()
        # arch_size = None , it will be img_size
        self.arch = G_arch(self.ch, self.attention)[config['arch_size']]
        # Which convs, batchnorms, and linear layers to use
        if config['G_SpectralNorm']:
            self.which_conv = functools.partial(modules.SNConv2d,
                                                kernel_size=3, padding=1,
                                                num_svs=config['num_G_SVs'], num_itrs=config['num_G_SV_itrs'],
                                                eps=self.SN_eps)
            self.which_linear = functools.partial(modules.SNLinear,
                                                  num_svs=config['num_G_SVs'], num_itrs=config['num_G_SV_itrs'],
                                                  eps=self.SN_eps)
        else:
            self.which_conv = functools.partial(nn.Conv2d, kernel_size=3, padding=1)
            self.which_linear = nn.Linear

        self.which_attn = None
        if config['attention']:
            if config['attn_style'] == 'cbam':
                self.which_attn = modules.CBAM
            else:
                self.which_attn = modules.Attention

        # If using hierarchical latents, adjust z
        if config['hierarchical']:
            # Number of places z slots into
            self.num_slots = len(self.arch['in_channels']) + 1
            self.z_chunk_size = (config['noise_dim'] // self.num_slots)
            # Recalculate latent dimensionality for even splitting into chunks
            self.dim_z = self.z_chunk_size * self.num_slots
        else:
            self.num_slots = 1
            self.z_chunk_size = 0

        self.linear1 = self.which_linear(config['noise_dim'] // self.num_slots,
                                        self.arch['in_channels'][0] * (self.bottom_width ** 2))

        ## add attention for dcgan
        # 8, 8, 128
        self.transposed_conv2 = modules.TransposedConv(128, 512, 5, 1,
                                                       padding=2, dilation=1)  # 8, 8, 512
        # attention

        self.dropout3 = nn.Dropout(config['dropout_rate'])
        self.transposed_conv4 = modules.TransposedConv(512, 256, 5, 2,
                                                       padding=2, dilation=1, output_padding=1)  # 16, 16, 256
        # attention
        self.dropout5 = nn.Dropout(config['dropout_rate'])
        self.transposed_conv6 = modules.TransposedConv(256, 128, 5, 2,
                                                       padding=2, dilation=1, output_padding=1)  # 32, 32, 128
        # attention
        self.transposed_conv7 = modules.TransposedConv(128, 64, 5, 2,
                                                       padding=2, dilation=1, output_padding=1)  # 64, 64, 64
        # attention
        self.transposed_conv8 = modules.TransposedConv(64, 32, 5, 2,
                                                       padding=2, dilation=1, output_padding=1)  # 128, 128, 32
        self.linear8 = nn.Linear(32, 3)  # 128, 128, 3