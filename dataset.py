import math
from functools import partial

import albumentations as A
import cv2
import numpy as np
import torch
import torchvision.transforms as T
from PIL import Image, ImageFile
from albumentations.pytorch import ToTensor
from numba import jit
from torch.utils.data import DataLoader
from torchvision import datasets
from tqdm import tqdm

import constants
from utils import PIL_read, get_annotator

ImageFile.LOAD_TRUNCATED_IMAGES = True

IMG_SIZE = 128
IMG_SIZE_2 = IMG_SIZE * 2
IMG_EXTENSIONS = ('.jpg', '.jpeg', '.png', '.ppm', '.bmp', '.pgm', '.tif', '.tiff', '.webp')


class MyImg:
    def __init__(self, img, tfm):
        self.px = np.array(img)
        self.tfm = tfm

    @property
    def size(self):
        h, w, _ = self.px.shape
        return min(w, h)


def pad(img, padding_mode='reflect'):
    p = math.ceil((max(img.size) - min(img.size)) / 2)
    p_horr = p if img.width < img.height else 0
    p_vert = p if img.height < img.width else 0
    img = T.Pad((p_horr, p_vert), padding_mode=padding_mode)(img)
    if img.width != img.height:
        s = min(img.size)
        img = img.crop((0, 0, s, s))
    return img


def take_top(img):
    size = min(img.size)
    bbox = (0, 0, size, size)
    return img.crop(bbox)


def take_diagonal(img):
    w, h = img.size
    size = min(w, h)
    bbox_l = (0, 0, size, size)
    bbox_r = (w - size, h - size, w, h)
    return [img.crop(bbox_l), img.crop(bbox_r)]


resize = T.Resize(IMG_SIZE, interpolation=Image.LANCZOS)
resize2x = T.Resize(IMG_SIZE_2, interpolation=Image.LANCZOS)

center_crop = T.Compose([resize, T.CenterCrop(IMG_SIZE)])
center_crop2x = T.Compose([resize2x, T.CenterCrop(IMG_SIZE_2)])

top_crop = T.Compose([T.Lambda(take_top), resize])
top_crop2x = T.Compose([T.Lambda(take_top), resize2x])

two_crops = T.Compose([resize, T.Lambda(take_diagonal)])
two_crops2x = T.Compose([resize2x, T.Lambda(take_diagonal)])

pad_only = T.Compose([T.Lambda(pad), resize])
pad_only2x = T.Compose([T.Lambda(pad), resize2x])


@jit(nopython=True)
def calc_one_axis(clow, chigh, pad, cmax):
    clow = max(0, clow - pad)
    chigh = min(cmax, chigh + pad)
    return clow, chigh, chigh - clow


def calc_bbox(obj, img_w, img_h, zoom=0.0, try_square=True):
    xmin = int(obj[0])
    ymin = int(obj[1])
    xmax = int(obj[2])
    ymax = int(obj[3])

    # occasionally i get bboxes which exceed img size
    xmin, xmax, obj_w = calc_one_axis(xmin, xmax, 0, img_w)
    ymin, ymax, obj_h = calc_one_axis(ymin, ymax, 0, img_h)

    if zoom != 0.0:
        pad_w = obj_w * zoom / 2
        pad_h = obj_h * zoom / 2
        xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad_w, img_w)
        ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad_h, img_h)

    if try_square:
        # try pad both sides equaly
        if obj_w > obj_h:
            pad = (obj_w - obj_h) / 2
            ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad, img_h)
        elif obj_h > obj_w:
            pad = (obj_h - obj_w) / 2
            xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad, img_w)

        # if it's still not square, try pad where possible
        if obj_w > obj_h:
            pad = obj_w - obj_h
            ymin, ymax, obj_h = calc_one_axis(ymin, ymax, pad, img_h)
        elif obj_h > obj_w:
            pad = obj_h - obj_w
            xmin, xmax, obj_w = calc_one_axis(xmin, xmax, pad, img_w)

    return int(xmin), int(ymin), int(xmax), int(ymax)


@jit(nopython=True)
def bb2wh(bbox):
    width = bbox[2] - bbox[0]
    height = bbox[3] - bbox[1]
    return width, height


def make_x2res(img, bbox):
    if min(bb2wh(bbox)) < IMG_SIZE_2:
        return
    ar = img.width / img.height
    if ar == 1.0:
        tfm_img = resize2x(img)
    elif 1.0 < ar < 1.15:
        tfm_img = center_crop2x(img)
    elif 1.15 < ar < 1.25:
        tfm_img = pad_only2x(img)
    elif 1.25 < ar < 1.5:
        tfm_img = two_crops2x(img)
    elif 1.0 < 1 / ar < 1.6:
        tfm_img = top_crop2x(img)
    else:
        tfm_img = None
    return tfm_img


def add_sample(samples, label, tfm, imgs, labels):
    if not samples:
        return
    elif isinstance(samples, Image.Image):
        imgs.append(MyImg(samples, tfm))
        labels.append(label)
    elif isinstance(samples, list):
        imgs.extend([MyImg(s, tfm) for s in samples])
        labels.extend([label] * len(samples))
    else:
        assert False


def is_valid_file(x):
    return datasets.folder.has_file_allowed_extension(x, IMG_EXTENSIONS)


class VehicleDataSet(datasets.vision.VisionDataset):
    def __init__(self, root, label_root, transforms, max_samples=None):
        super().__init__(root, transform=None)
        assert isinstance(transforms, list) and len(transforms) == 3
        self.transforms = transforms
        self.max_samples = max_samples
        self.classes = {}

        imgs, labels = self._load_subfolders_images(self.root, label_root)
        assert len(imgs) == len(labels)
        if len(imgs) == 0:
            raise RuntimeError('Found 0 files in subfolders of: {}'.format(self.root))
        self.imgs = imgs
        self.labels = labels

    def _create_or_get_class(self, name):
        try:
            label = self.classes[name]
        except KeyError:
            label = len(self.classes)
            self.classes[name] = label
        return label

    def _load_subfolders_images(self, root, label_root):
        light_zoom, medium_zoom = 0.08, 0.12
        n_pad, n_center, n_top, n_2crops, n_skip, n_dup, n_noop = 0, 0, 0, 0, 0, 0, 0
        imgs, labels, paths = [], [], []

        add_sample_ = partial(add_sample, imgs=imgs, labels=labels)
        label_mapper = get_annotator(label_root)

        for path in label_mapper:
            paths.append(path)

        if self.max_samples:
            paths = paths[:self.max_samples]

        for path in tqdm(paths):
            if not is_valid_file(path):
                continue
            img = PIL_read(path)
            annotation = label_mapper[path]
            for anno_bbox in annotation:
                prev_bbox, tfm_imgs = None, None

                bbox = calc_bbox(anno_bbox, img_w=img.width, img_h=img.height, zoom=light_zoom)
                obj_img = img.crop(bbox)
                add_sample_(make_x2res(obj_img, bbox), 0, 2)

                bbox = calc_bbox(anno_bbox, img_w=img.width, img_h=img.height)
                if min(bb2wh(bbox)) < IMG_SIZE:
                    # don't want pixel mess in gen imgs
                    n_skip += 1
                    continue
                obj_img = img.crop(bbox)
                ar = obj_img.width / obj_img.height
                if ar == 1.0:  # image have be square
                    tfm_imgs = [resize(obj_img)]
                    n_noop += 1
                elif 1.0 < ar < 1.3:  # change s
                    tfm_imgs = [center_crop(obj_img), pad_only(obj_img)]
                    n_center += 1
                    n_pad += 1
                elif 1.3 <= ar < 1.5:
                    tfm_imgs = two_crops(obj_img) + [pad_only(obj_img)]
                    n_2crops += 2
                    n_pad += 1
                elif 1.5 <= ar < 2.0:
                    tfm_imgs = two_crops(obj_img)
                    n_2crops += 2
                elif 1.0 < 1 / ar < 1.5:
                    tfm_imgs = [top_crop(obj_img), pad_only(obj_img)]
                    n_top += 1
                    n_pad += 1
                elif 1.5 <= 1 / ar < 1.8:
                    tfm_imgs = [top_crop(obj_img)]
                    n_top += 1
                else:
                    tfm_imgs = None
                    n_skip += 1
                add_sample_(tfm_imgs, 0, 0)
                add_sample_(make_x2res(obj_img, bbox), 0, 1)
                prev_bbox = bbox

                bbox = calc_bbox(anno_bbox, img_w=img.width, img_h=img.height, zoom=medium_zoom, try_square=False)
                if bbox == prev_bbox:
                    n_dup += 1
                    continue
                if min(bb2wh(bbox)) < IMG_SIZE_2: continue
                obj_img = img.crop(bbox)
                ar = obj_img.width / obj_img.height
                if 1.3 < ar < 1.5:
                    tfm_imgs = two_crops(obj_img)
                    n_2crops += 2
                elif 1.05 < 1 / ar < 1.6:  # maybe tall
                    tfm_imgs = top_crop(obj_img)
                    n_top += 1
                else:
                    continue
                add_sample_(tfm_imgs, 0, 0)
                add_sample_(make_x2res(obj_img, bbox), 0, 1)
                prev_bbox = bbox

        n_x1, n_x2 = 0, 0
        for i, img in enumerate(imgs):
            if img.size == IMG_SIZE:
                n_x1 += 1
            else:
                n_x2 += 1

        print('Loaded 128x128 {} images\nLoaded 256x256 {} images\n'.format(n_x1, n_x2))
        print('Pad only: {}\nCrop center: {}\n'
              'Crop top: {}\nCrop 2 times: {}\n'
              'Take as-is: {}\nSkip: {}\nSame bbox: {}' \
              .format(n_pad, n_center, n_top, n_2crops, n_noop, n_skip, n_dup))
        del label_mapper
        return imgs, labels

    def __getitem__(self, index):
        img = self.imgs[index]
        label = self.labels[index]
        tfms = self.transforms[img.tfm]
        img = tfms(image=img.px)['image']
        # if self.target_transform:
        #     label = self.target_transform(label)
        return img, label

    def __len__(self):
        return len(self.imgs)


def create_runtime_tfms1():
    print('Data will be augmented...%d' % 1)
    mean, std = [0.5] * 3, [0.5] * 3
    resize_to_128 = A.SmallestMaxSize(IMG_SIZE, interpolation=cv2.INTER_AREA)
    out = [A.HorizontalFlip(p=0.5), A.Normalize(mean=mean, std=std), ToTensor()]

    rand_crop = A.Compose([
        A.SmallestMaxSize(IMG_SIZE + 8, interpolation=cv2.INTER_AREA),
        A.RandomCrop(IMG_SIZE, IMG_SIZE)
    ])

    affine_1 = A.ShiftScaleRotate(
        shift_limit=0, scale_limit=0.1, rotate_limit=8,
        interpolation=cv2.INTER_CUBIC,
        border_mode=cv2.BORDER_REFLECT_101, p=1.0)
    affine_1 = A.Compose([affine_1, resize_to_128])

    affine_2 = A.ShiftScaleRotate(
        shift_limit=0.06, scale_limit=(-0.06, 0.18), rotate_limit=6,
        interpolation=cv2.INTER_CUBIC,
        border_mode=cv2.BORDER_REFLECT_101, p=1.0)
    affine_2 = A.Compose([affine_2, resize_to_128])

    tfm_0 = A.Compose(out)
    tfm_1 = A.Compose([A.OneOrOther(affine_1, rand_crop, p=1.0), *out])
    tfm_2 = A.Compose([affine_2, *out])
    return [tfm_0, tfm_1, tfm_2]


class RandomCropLongEdge(object):
    """Crops the given PIL Image on the long edge with a random start point.
    Args:
        size (sequence or int): Desired output size of the crop. If size is an
            int instead of sequence like (h, w), a square crop (size, size) is
            made.
    """

    def __call__(self, img):
        """
        Args:
            img (PIL Image): Image to be cropped.
        Returns:
            PIL Image: Cropped image.
        """
        size = (min(img.size), min(img.size))
        # Only step forward along this edge if it's the long edge
        i = (0 if size[0] == img.size[0]
             else np.random.randint(low=0, high=img.size[0] - size[0]))
        j = (0 if size[1] == img.size[1]
             else np.random.randint(low=0, high=img.size[1] - size[1]))
        return T.functional.crop(img, j, i, size[0], size[1])

    def __repr__(self):
        return self.__class__.__name__


def create_runtime_tfms2():
    print('Data will be augmented...%d' % 2)
    train_transform = [
        RandomCropLongEdge(),
        A.SmallestMaxSize(IMG_SIZE, interpolation=cv2.INTER_AREA),
        T.RandomHorizontalFlip()]
    return train_transform


def create_runtime_tfms2_(resize_mode):
    print('Data will be augmented...%d' % 21)
    train_transform = [
        RandomCropLongEdge(),
        T.Resize(IMG_SIZE, resize_mode),
        T.RandomHorizontalFlip()]
    return train_transform


def create_runtime_tfms3():
    print('Data will be augmented...%d' % 3)
    train_transform = [
        T.RandomRotation(5),
        T.RandomHorizontalFlip()]
    return train_transform


def create_runtime_tfms4():
    print('Data will be augmented...%d' % 4)
    train_transform = [
        T.RandomRotation(8),
        T.RandomHorizontalFlip()]
    return train_transform


def create_runtime_tfms5():
    print('Data will be augmented...%d' % 5)
    train_transform = [
        T.RandomRotation(10),
        T.RandomHorizontalFlip()]
    return train_transform


def create_runtime_tfms6():
    print('Data will be augmented...%d' % 6)
    resize_trans1 = T.Compose(
        [RandomCropLongEdge(), A.SmallestMaxSize(IMG_SIZE, interpolation=cv2.INTER_AREA)])
    resize_trans2 = A.SmallestMaxSize(IMG_SIZE, interpolation=cv2.INTER_AREA)
    resize_trans = T.RandomChoice([resize_trans1, resize_trans2])
    train_transform = [resize_trans, T.RandomHorizontalFlip()]
    return train_transform


def create_runtime_tfms6_(resize_mode):
    print('Data will be augmented...%d' % 61)
    resize_trans1 = T.Compose([RandomCropLongEdge(), T.Resize(IMG_SIZE, resize_mode)])
    resize_trans2 = T.Resize([IMG_SIZE, IMG_SIZE], resize_mode)
    resize_trans = T.RandomChoice([resize_trans1, resize_trans2])
    train_transform = [resize_trans, T.RandomHorizontalFlip()]
    return train_transform


# multi-epoch Dataset sampler to avoid memory leakage and enable resumption of
# training from the same sample regardless of if we stop mid-epoch
class MultiEpochSampler(torch.utils.data.Sampler):
    r"""Samples elements randomly over multiple epochs

    Arguments:
        data_source (Dataset): dataset to sample from
        num_epochs (int) : Number of times to loop over the dataset
        start_itr (int) : which iteration to begin from
    """

    def __init__(self, data_source, num_epochs, start_itr=0, batch_size=128):
        self.data_source = data_source
        self.num_samples = len(self.data_source)
        self.num_epochs = num_epochs
        self.start_itr = start_itr
        self.batch_size = batch_size

        if not isinstance(self.num_samples, int) or self.num_samples <= 0:
            raise ValueError("num_samples should be a positive integeral "
                             "value, but got num_samples={}".format(self.num_samples))

    def __iter__(self):
        n = len(self.data_source)
        # Determine number of epochs
        num_epochs = int(np.ceil((n * self.num_epochs
                                  - (self.start_itr * self.batch_size)) / float(n)))
        # Sample all the indices, and then grab the last num_epochs index sets;
        # This ensures if we're starting at epoch 4, we're still grabbing epoch 4's
        # indices
        out = [torch.randperm(n) for epoch in range(self.num_epochs)][-num_epochs:]
        # Ignore the first start_itr % n indices of the first epoch
        out[0] = out[0][(self.start_itr * self.batch_size % n):]
        # if self.replacement:
        # return iter(torch.randint(high=n, size=(self.num_samples,), dtype=torch.int64).tolist())
        # return iter(.tolist())
        output = torch.cat(out).tolist()
        print('Length dataset output is %d' % len(output))
        return iter(output)

    def __len__(self):
        return len(self.data_source) * self.num_epochs - self.start_itr * self.batch_size


def get_data_loaders(data_root=None, label_root=None, batch_size=32,
                     augment=1, num_epochs=500,
                     num_workers=2, shuffle=True, pin_memory=True, drop_last=True,
                     use_multiepoch_sampler=None, start_itr=None, **kwargs):
    print('Using dataset root location %s' % data_root)
    print('Augument 3, 4, 5 have just supported for 128-images before !!!')
    transform = None
    if augment == 1:
        transform = create_runtime_tfms1()
    elif augment == 2:
        transform = create_runtime_tfms2()
    elif augment == 21:
        transform = create_runtime_tfms2_(kwargs.get('resize_mode'))
    elif augment == 3:
        if kwargs.get('resolution') != IMG_SIZE:
            raise ValueError("Augument {} has note supported for {}".format(augment, kwargs.get('resolution')))
        transform = create_runtime_tfms3()
    elif augment == 4:
        if kwargs.get('resolution') != IMG_SIZE:
            raise ValueError("Augument {} has note supported for {}".format(augment, kwargs.get('resolution')))
        transform = create_runtime_tfms4()
    elif augment == 5:
        if kwargs.get('resolution') != IMG_SIZE:
            raise ValueError("Augument {} has note supported for {}".format(augment, kwargs.get('resolution')))
        transform = create_runtime_tfms5()
    elif augment == 6:
        transform = create_runtime_tfms6()
    elif augment == 61:
        transform = create_runtime_tfms6_(kwargs.get('resize_mode'))

    train_set = VehicleDataSet(data_root, label_root, transform)
    # Prepare loader; the loaders list is for forward compatibility with
    # using validation / test splits.
    loaders = []
    if use_multiepoch_sampler:
        print('Using multiepoch sampler from start_itr %d...' % start_itr)
        loader_kwargs = {'num_workers': num_workers, 'pin_memory': pin_memory}
        sampler = MultiEpochSampler(train_set, num_epochs, start_itr,
                                    batch_size)
        train_loader = DataLoader(train_set, batch_size=batch_size,
                                  sampler=sampler, **loader_kwargs)
    else:
        loader_kwargs = {'num_workers': num_workers, 'pin_memory': pin_memory,
                         'drop_last': drop_last}  # Default, drop last incomplete batch
        train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=shuffle, **loader_kwargs)
    loaders.append(train_loader)
    return loaders


def data_preparation(annotation_file, image_size, resize_flag=False):
    light_zoom, medium_zoom = 0.08, 0.12
    label_mapper = get_annotator(annotation_file)
    for path in tqdm(label_mapper):
        image = PIL_read(path)
        width, height = image.size
        annotation = label_mapper[path]
        for i, anno_bbox in enumerate(annotation):
            prev_bbox, tfm_imgs = None, None
            bbox = calc_bbox(anno_bbox, img_w=width, img_h=height, zoom=light_zoom)
            ratio = (bbox[3] - bbox[1]) / (bbox[2] - bbox[0])

            if ratio < 0.2 or ratio > 4.0:
                continue

            img_cropped = image.crop(bbox)  # [h,w,c]
            if resize_flag:
                # Interpolation method
                if bbox[2] - bbox[0] > image_size:
                    interpolation = Image.ANTIALIAS  # shrink
                else:
                    interpolation = Image.CUBIC  # expansion

                img_cropped = img_cropped.resize((image_size, image_size), interpolation)
            img_cropped.save(constants.processed_path + str(i) + "_" + path.split('/')[-1])


def data_preparation1(annotation_file, image_size, resize_flag=False):
    label_mapper = get_annotator(annotation_file)
    for path in tqdm(label_mapper):
        image = PIL_read(path)
        width, height = image.size
        annotation = label_mapper[path]
        for i, anno_bbox in enumerate(annotation):
            xmin = int(anno_bbox[0])
            ymin = int(anno_bbox[1])
            xmax = int(anno_bbox[2])
            ymax = int(anno_bbox[3])

            xmin = max(0, xmin - 3)  # 4 : margin
            xmax = min(width, xmax + 3)
            ymin = max(0, ymin - 10)
            ymax = min(height, ymax + 10)

            w = np.max((xmax - xmin, ymax - ymin))
            w = min(w, width, height)  # available w

            # ratio = (ymax - ymin) / (xmax - xmin)
            # if ratio < 0.2 or ratio > 4.0:
            #   return None
            if xmax - xmin > ymax - ymin:
                if w > xmax - xmin:
                    xmin = min(max(0, xmin - int((w - (xmax - xmin)) / 2)), width - w)
                    xmax = xmin + w
                if w > ymax - ymin:
                    ymin = min(max(0, ymin - int((w - (ymax - ymin)) / 2)), height - w)
                    ymax = ymin + w

            # img_cropped = image.crop((xmin, ymin, w, w))  # [h,w,c]
            # img_cropped =
            img_cropped = image.crop((xmin, ymin, xmax, ymax))  # [h,w,c]
            # Interpolation method
            if resize_flag:
                if xmax - xmin > image_size:
                    interpolation = Image.ANTIALIAS  # shrink
                else:
                    interpolation = Image.CUBIC  # expansion

                img_cropped = img_cropped.resize((image_size, image_size), interpolation)
            img_cropped.save(constants.processed_path + str(i) + "_" + path.split('/')[-1])


def data_preparation2(annotation_file, image_size=None, x_overlap_threshold=10, y_overlap_threshold=20):
    label_mapper = get_annotator(annotation_file)
    for path in tqdm(label_mapper):
        image = PIL_read(path)
        width, height = image.size
        annotation = label_mapper[path]
        for i, anno_bbox in enumerate(annotation):
            xmin = int(anno_bbox[0])
            ymin = int(anno_bbox[1])
            xmax = int(anno_bbox[2])
            ymax = int(anno_bbox[3])

            remaining_bboxs = annotation[:i] + annotation[i + 1:]
            if xmax - xmin > ymax - ymin:
                offset = xmax - xmin - (ymax - ymin)
                nearest_bottom = 0
                nearest_under = height
                for bbox in remaining_bboxs:
                    r_xmin = int(bbox[0])
                    r_ymin = int(bbox[1])
                    r_xmax = int(bbox[2])
                    r_ymax = int(bbox[3])
                    overlap_seg = min(xmax, r_xmax) - max(xmin, r_xmin)
                    if overlap_seg < 0 or overlap_seg - x_overlap_threshold < 0:
                        if r_ymax < ymin:
                            nearest_bottom = max(nearest_bottom, r_ymax)
                        elif r_ymin > ymax:
                            nearest_under = min(nearest_under, r_ymin)
                bottom_offset = min(offset / 2, ymin - nearest_bottom)
                under_offset = min(offset / 2, nearest_under - ymax)
                if ymin - bottom_offset < 0:
                    under_offset += bottom_offset - ymin
                    bottom_offset = ymin
                if ymax + under_offset > height:
                    bottom_offset += ymax + under_offset - height
                    under_offset = height - ymax

                ymin -= bottom_offset
                ymax += under_offset

            else:
                offset = ymax - ymin - (xmax - xmin)
                if (ymax - ymin) / (xmax - xmin) > 3:
                    continue
                nearest_left = 0
                nearest_right = width

                for bbox in remaining_bboxs:
                    r_xmin = int(bbox[0])
                    r_ymin = int(bbox[1])
                    r_xmax = int(bbox[2])
                    r_ymax = int(bbox[3])
                    overlap_seg = min(ymax, r_ymax) - max(ymin, r_ymin)
                    if overlap_seg < 0 or overlap_seg - y_overlap_threshold < 0:
                        if r_xmax < xmin:
                            nearest_left = max(nearest_left, r_xmax)
                        elif r_xmin > xmax:
                            nearest_right = min(nearest_right, r_xmin)

                left_offset = min(offset / 2, xmin - nearest_left)
                right_offset = min(offset / 2, nearest_right - xmax)
                if xmin - left_offset < 0:
                    right_offset += left_offset - xmin
                    left_offset = xmin
                if xmax + right_offset > width:
                    left_offset += xmax + right_offset - width
                    right_offset = width - xmax

                xmin -= left_offset
                xmax += right_offset

            if (xmax - xmin) > (ymax - ymin):
                img_cropped = image.crop((xmin, ymin, xmax, ymax))
                white_image = Image.new('RGB',
                                        ((xmax - xmin), (xmax - xmin)),  # A4 at 72dpi
                                        (255, 255, 255))  # White
                white_image.paste(img_cropped, (0, int(((xmax - xmin) - (ymax - ymin)) / 2)))
                img_cropped = white_image
            elif (xmax - xmin) < (ymax - ymin):
                if (ymax - ymin) / (xmax - xmin) > 3.5:
                    continue
                ymin += 15
                ymax -= 5
                img_cropped = image.crop((xmin, ymin, xmax, ymax))
                min_ = min(xmax - xmin, ymax - ymin)
                max_ = max(xmax - xmin, ymax - ymin)
                white_image = Image.new('RGB',
                                        (max_, max_),  # A4 at 72dpi
                                        (255, 255, 255))  # White
                white_image.paste(img_cropped, (0, int((max_ - min_) / 2)))
                img_cropped = white_image
            # Interpolation method
            if image_size:
                if xmax - xmin > image_size:
                    interpolation = Image.ANTIALIAS  # shrink
                else:
                    interpolation = Image.CUBIC  # expansion

                img_cropped = img_cropped.resize((image_size, image_size), interpolation)
            img_cropped.save(constants.processed_path + str(i) + "_" + path.split('/')[-1])
