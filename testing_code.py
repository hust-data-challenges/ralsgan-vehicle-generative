from src.utils.config_parser import cfg_from_file
from src.datasets.dataset_factory import get_loader
from src.models.generator_factory import get_generator
from src.models.discriminator_factory import get_discriminator
from src.optimizer_factory import get_discriminator_optimizer, get_generator_optimizer

if __name__ == '__main__':
    config = cfg_from_file("./yml_config/training_1.yml")
    config.model.scale_factor = config.model.scale_factor ** config.model.downsize_factor
    # dataset = get_loader(config.dataset, config.common)
    # vehicle_generator = get_generator(config.model, config.common)
    # vehicle_discriminator = get_discriminator(config.model, config.common)

    g_optimizer = get_generator_optimizer(config.optimizer, config.common)
    d_optimizer = get_discriminator_optimizer(config.optimizer, config.common)