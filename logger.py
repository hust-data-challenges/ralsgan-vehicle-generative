from tensorboardX import SummaryWriter
from utils import makedirs
import os

import constants


class MyLogger:
    def __init__(self, experiment_name):
        makedirs(constants.logs_root + experiment_name)
        self.writer_G = SummaryWriter(logdir=constants.logs_root + experiment_name + "/G")
        self.writer_D_real = SummaryWriter(logdir=constants.logs_root + experiment_name + "/D_real")
        self.writer_D_fake = SummaryWriter(logdir=constants.logs_root + experiment_name + "/D_fake")
        self.fid = SummaryWriter(logdir=constants.logs_root + experiment_name + "/fid")
        self.distance = SummaryWriter(logdir=constants.logs_root + experiment_name + "/distance")
        self.mifid = SummaryWriter(logdir=constants.logs_root + experiment_name + "/mifid")

    def write(self, metrics, scores, itr):
        self.writer_G.add_scalar('loss', metrics['G_loss'], int(itr))
        self.writer_D_real.add_scalar('loss', metrics['D_loss_real'], int(itr))
        self.writer_D_fake.add_scalar('loss', metrics['D_loss_fake'], int(itr))
        self.fid.add_scalar('evaluation_score', scores['fid'], int(itr))
        self.distance.add_scalar('evaluation_score', scores['distance'], int(itr))
        self.mifid.add_scalar('evaluation_score', scores['mifid'], int(itr))
