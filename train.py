import datetime
import functools
import time

import torch
from tqdm import tqdm

import config_parser
import constants
import dataset
import inception_utils
import self_attention
import train_fns
import utils
from ema import Ema
from evaluation.client.mifid_demo import MIFID
from logger import MyLogger


def run(config, print_model=False):
    config['resolution'] = 128
    if config['resume']:
        print('Skipping initialization for training resumption...')
        config['skip_init'] = True

    config = utils.update_config_roots(config)
    device = 'cuda'

    # Prepare root folders if necessary
    utils.prepare_root()

    # Seed RNG
    utils.seed_rng(config['seed'])

    # Setup cudnn.benchmark for free speed
    torch.backends.cudnn.benchmark = True

    experiment_name = (config['experiment_name'] if config['experiment_name'] + "/"
                       else 'generative_dog_images/')
    print('Experiment name is %s' % experiment_name)

    model = __import__(config['model'])
    G = self_attention.Generator(config).to(device)
    D = self_attention.Discriminator(config).to(device)

    if print_model:
        print(G)
        print(D)

    # If using EMA, prepare it
    if config['ema']:
        print('Preparing EMA for G with decay of {}'.format(config['ema_decay']))
        G_ema = model.Generator(**{**config, 'skip_init': True,
                                   'no_optim': True}).to(device)
        ema = Ema(G, G_ema, config['ema_decay'], config['ema_start'])
    else:
        G_ema, ema = None, None

    # FP16?
    if config['G_fp16']:
        print('Casting G to float16...')
        G = G.half()
        if config['ema']:
            G_ema = G_ema.half()
    if config['D_fp16']:
        print('Casting D to fp16...')
        D = D.half()

    GD = model.G_D(G, D)
    print('Number of params in G: {} D: {}'.format(
        *[sum([p.data.nelement() for p in net.parameters()]) for net in [G, D]]))
    # Prepare state dict, which holds things like epoch # and itr #
    state_dict = {'itr': 0, 'epoch': 0, 'save_num': 0, 'config': config,
                  'best_IS': 0, 'best_FID': 999999, 'save_best_num': 0}

    # If loading from a pre-trained model, load weights
    if config['resume']:
        print('Loading weights...')
        utils.load_weights(G, D, state_dict,
                           constants.checkpoints_root, experiment_name,
                           config['load_weights'] if config['load_weights'] else None,
                           G_ema if config['ema'] else None)
        if G.lr_sched is not None: G.lr_sched.step(state_dict['epoch'])
        if D.lr_sched is not None: D.lr_sched.step(state_dict['epoch'])

    logger = MyLogger(experiment_name)

    # Prepare data; the Discriminator's batch size is all that needs to be passed
    # to the dataloader, as G doesn't require dataloading.
    # Note that at every loader iteration we pass in enough data to complete
    # a full D iteration (regardless of number of D steps and accumulations)
    kwargs = {'resolution': config['resolution'], 'n_classes': config['n_classes'],
              'resize_mode': config['resize_mode']}
    D_batch_size = (config['batch_size'] * config['num_D_steps'] * config['num_D_accumulations'])
    loaders = dataset.get_data_loaders(
        data_root=config['data_root'],
        label_root=config['label_root'],
        batch_size=D_batch_size,
        num_workers=config['num_workers'],
        shuffle=config['shuffle'],
        pin_memory=config['pin_memory'],
        drop_last=True,
        **kwargs)

    # evaluator = MIFID(constants.evaluation_model, constants.public_feature_path)
    if not config['zalo']:
        # Prepare inception metrics: FID and IS
        evaluator = inception_utils.prepare_inception_metrics(config['base_root'],
                                                              config['dataset'],
                                                              config['parallel'],
                                                              config['no_fid'])

    # Prepare noise and randomly sampled label arrays
    # Allow for different batch sizes in G
    G_batch_size = max(config['G_batch_size'], config['batch_size'])

    dim_z = G.dim_z * 2 if config['mix_style'] else G.dim_z
    z_, y_ = utils.prepare_z_y(
        G_batch_size, dim_z, config['n_classes'], device=device, fp16=config['G_fp16'])
    # Prepare a fixed z & y to see individual sample evolution throghout training
    fixed_z, fixed_y = utils.prepare_z_y(
        G_batch_size, dim_z, config['n_classes'], device=device, fp16=config['G_fp16'])

    fixed_z.sample_()
    fixed_y.sample_()
    # Loaders are loaded, prepare the training function
    train = train_fns.create_train_fn(G, D, GD, z_, y_, ema, state_dict, config)
    # Prepare Sample function for use with inception metrics
    sample = functools.partial(utils.sample,
                               G=(G_ema if config['ema'] and config['use_ema']
                                  else G),
                               z_=z_, y_=y_, config=config)
    print('Beginning training at epoch %d...' % state_dict['epoch'])
    by_epoch = False if config['save_every'] > 100 else True
    start_time = time.perf_counter()
    total_iters = config['num_epochs'] * len(loaders[0])
    scores = {'fid': 99999, 'distance': -1, 'mifid': 99999}
    # Train for specified number of epochs, although we mostly track G iterations.
    for epoch in range(state_dict['epoch'], config['num_epochs']):
        # Which progressbar to use? TQDM or my own?
        if config['on_kaggle']:
            pbar = loaders[0]
        elif config['pbar'] == 'mine':
            pbar = utils.progress(loaders[0], displaytype='s1k' if config['use_multiepoch_sampler'] else 'eta')
        else:
            pbar = tqdm(loaders[0])

        epoch_start_time = time.time()
        for i, (x, y) in enumerate(pbar):
            # Increment the iteration counter
            state_dict['itr'] += 1
            # Make sure G and D are in training mode, just in case they got set to eval
            # For D, which typically doesn't have BN, this shouldn't matter much.
            G.train()
            D.train()
            if config['ema']:
                G_ema.train()

            if config['D_fp16']:
                x, y = x.to(device).half(), y.to(device)
            else:
                x, y = x.to(device), y.to(device)
            metrics = train(x, y)

            # if not (state_dict['itr'] % config['log_interval']):
            #     curr_time = time.perf_counter()
            #     curr_time_str = datetime.datetime.fromtimestamp(curr_time).strftime('%H:%M:%S')
            #     elapsed = str(datetime.timedelta(seconds=(curr_time - start_time)))
            #     log = (
            #             "[{}] [{}] [{} / {}] Ep {}, ".format(curr_time_str, elapsed, state_dict['itr'], total_iters,
            #                                                  epoch) +
            #             ', '.join(['%s : %+4.3f' % (key, metrics[key]) for key in metrics])
            #     )
            #     print(log)

            # If using my progbar, print metrics.
            if config['on_kaggle']:
                if i == len(loaders[0]) - 1:
                    metrics_str = ', '.join(['%s : %+4.3f' % (key, metrics[key]) for key in metrics])
                    epoch_time = (time.time() - epoch_start_time) / 60
                    total_time = (time.time() - start_time) / 60
                    print(
                        "[{}/{}][{:.1f}min/{:.1f}min] {}" \
                            .format(epoch + 1, config['num_epochs'], epoch_time, total_time, metrics_str))
            elif config['pbar'] == 'mine':
                if D.lr_sched is None:
                    print(', '.join(['Epoch:%d' % (epoch + 1), 'Itr: %d' % state_dict['itr']]
                                    + ['%s : %+4.3f' % (key, metrics[key])
                                       for key in metrics]), end=' ')
                else:
                    print(', '.join(['Epoch:%d' % (epoch + 1), 'lr:%.5f' % D.lr_sched.get_lr()[0],
                                     'Itr: %d' % state_dict['itr']]
                                    + ['%s : %+4.3f' % (key, metrics[key])
                                       for key in metrics]), end=' ')

            if not by_epoch:
                # Save weights and copies as configured at specified interval
                if not (state_dict['itr'] % config['save_every']) and not config['on_kaggle']:
                    if config['G_eval_mode']:
                        print('Switching G to eval mode...')
                        G.eval()
                        if config['ema']:
                            G_ema.eval()
                    train_fns.save_and_sample(G, D, G_ema, z_, y_, fixed_z, fixed_y,
                                              state_dict, config, experiment_name)

                # Test every specified interval
                if not (state_dict['itr'] % config['test_every']) and not config['on_kaggle']:
                    if config['G_eval_mode']:
                        print('Switching G to eval mode...')
                        G.eval()
                    curr_time = time.time()
                    if config['zalo']:
                        evaluator = MIFID(constants.evaluation_model, constants.public_feature_path)
                        train_fns.za_test(G, D, G_ema, z_, y_, state_dict, config, sample,
                                          evaluator, experiment_name, scores)
                        del evaluator
                    else:
                        train_fns.test(G, D, G_ema, z_, y_, state_dict, config, sample,
                                       evaluator, experiment_name, scores)
                    elapsed = str(datetime.timedelta(seconds=(time.perf_counter() - curr_time)))
                    fid_value = scores['fid']
                    mifid = scores['distance']
                    distance = scores['mifid']
                    log = (
                            "[{}] [{} / {}] Epoch {}: ".format(elapsed, state_dict['itr'], total_iters,
                                                               epoch) +
                            ', '.join(["fid: {:.4}".format(fid_value), "distance: {:.4}".format(distance),
                                       "mifid: {:.4}".format(mifid)])
                    )
                    print(log)

            logger.write(metrics, scores, state_dict['itr'])

        if by_epoch:
            # Save weights and copies as configured at specified interval
            if not ((epoch + 1) % config['save_every']) and not config['on_kaggle']:
                if config['G_eval_mode']:
                    print('Switching G to eval mode...')
                    G.eval()
                    if config['ema']:
                        G_ema.eval()
                train_fns.save_and_sample(G, D, G_ema, z_, y_, fixed_z, fixed_y,
                                          state_dict, config, experiment_name)

            # Test every specified interval
            if not ((epoch + 1) % config['test_every']) and not config['on_kaggle']:
                if config['G_eval_mode']:
                    print('Switchin G to eval mode...')
                    G.eval()
                train_fns.test(G, D, G_ema, z_, y_, state_dict, config, sample,
                               evaluator, experiment_name, scores)

            if G_ema is not None and (epoch + 1) % config['test_every'] == 0 and not config['on_kaggle']:
                torch.save(G_ema.state_dict(), '%s/%s/G_ema_epoch_%03d.pth' %
                           (constants.checkpoints_root, config['experiment_name'], epoch + 1))
        # Increment epoch counter at end of epoch
        state_dict['epoch'] += 1
        if G.lr_sched is not None:
            G.lr_sched.step()
        if D.lr_sched is not None:
            D.lr_sched.step()


def main():
    # parse command line and run
    parser = config_parser.prepare_parser()
    config = vars(parser.parse_args())
    print(config)
    run(config)


if __name__ == '__main__':
    main()
