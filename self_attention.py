import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from torch.nn import init

import modules


class Self_Attn(nn.Module):
    """ Self attention Layer"""

    def __init__(self, in_dim, activation):
        super(Self_Attn, self).__init__()
        self.chanel_in = in_dim
        self.activation = activation

        self.query_conv = nn.Conv2d(in_channels=in_dim, out_channels=in_dim // 8, kernel_size=1)
        self.key_conv = nn.Conv2d(in_channels=in_dim, out_channels=in_dim // 8, kernel_size=1)
        self.value_conv = nn.Conv2d(in_channels=in_dim, out_channels=in_dim, kernel_size=1)
        self.gamma = nn.Parameter(torch.zeros(1))

        self.softmax = nn.Softmax(dim=-1)  #

    def forward(self, x):
        """
            inputs :
                x : input feature maps( B X C X W X H)
            returns :
                out : self attention value + input feature
                attention: B X N X N (N is Width*Height)
        """
        m_batchsize, C, width, height = x.size()
        proj_query = self.query_conv(x).view(m_batchsize, -1, width * height).permute(0, 2, 1)  # B X CX(N)
        proj_key = self.key_conv(x).view(m_batchsize, -1, width * height)  # B X C x (*W*H)
        energy = torch.bmm(proj_query, proj_key)  # transpose check
        attention = self.softmax(energy)  # BX (N) X (N)
        proj_value = self.value_conv(x).view(m_batchsize, -1, width * height)  # B X C X N

        out = torch.bmm(proj_value, attention.permute(0, 2, 1))
        out = out.view(m_batchsize, C, width, height)

        out = self.gamma * out + x
        return out, attention


class Generator(nn.Module):
    """Generator."""

    def __init__(self, config, batch_size=64, image_size=128, z_dim=100, conv_dim=64, **kwargs):
        super(Generator, self).__init__()
        self.imsize = image_size
        layer1 = []
        layer2 = []
        layer3 = []
        last = []

        repeat_num = int(np.log2(self.imsize)) - 3
        mult = 2 ** repeat_num  # 8
        layer1.append(modules.SNTranspose2d(z_dim, conv_dim * mult, 4))
        layer1.append(nn.BatchNorm2d(conv_dim * mult))
        layer1.append(nn.ReLU())

        curr_dim = conv_dim * mult

        layer2.append(modules.SNTranspose2d(curr_dim, int(curr_dim / 2), 4, 2, 1))
        layer2.append(nn.BatchNorm2d(int(curr_dim / 2)))
        layer2.append(nn.ReLU())

        curr_dim = int(curr_dim / 2)

        layer3.append(modules.SNTranspose2d(curr_dim, int(curr_dim / 2), 4, 2, 1))
        layer3.append(nn.BatchNorm2d(int(curr_dim / 2)))
        layer3.append(nn.ReLU())

        if self.imsize == 64:
            layer4 = []
            curr_dim = int(curr_dim / 2)
            layer4.append(modules.SNTranspose2d(curr_dim, int(curr_dim / 2), 4, 2, 1))
            layer4.append(nn.BatchNorm2d(int(curr_dim / 2)))
            layer4.append(nn.ReLU())
            self.l4 = nn.Sequential(*layer4)
            curr_dim = int(curr_dim / 2)

        self.l1 = nn.Sequential(*layer1)
        self.l2 = nn.Sequential(*layer2)
        self.l3 = nn.Sequential(*layer3)

        last.append(nn.ConvTranspose2d(curr_dim, 3, 4, 2, 1))
        last.append(nn.Tanh())
        self.last = nn.Sequential(*last)

        self.attn1 = Self_Attn(128, 'relu')
        self.attn2 = Self_Attn(64, 'relu')
        self.init = config['G_weight_init']
        if not config['skip_init']:
            self.init_weights()

        if config['no_optim']:
            return
        self.lr, self.B1, self.B2, self.adam_eps = config['G_lr'], config['G_beta1'], config['G_beta2'], config[
            'adam_eps']
        if config['G_mixed_precision']:
            print('Using fp16 adam in G...')
            import utils
            self.optim = utils.Adam16(params=self.parameters(), lr=self.lr,
                                      betas=(self.B1, self.B2), weight_decay=0,
                                      eps=self.adam_eps, amsgrad=kwargs['amsgrad'])
        else:
            self.optim = optim.Adam(params=self.parameters(), lr=self.lr,
                                    betas=(self.B1, self.B2), weight_decay=0,
                                    eps=self.adam_eps, amsgrad=kwargs['amsgrad'])
        if config['sched_version'] == 'default':
            self.lr_sched = None
        elif config['sched_version'] == 'cal_v0':
            self.lr_sched = optim.lr_scheduler.CosineAnnealingLR(self.optim,
                                                                 T_max=config['epochs'], eta_min=self.lr / 2,
                                                                 last_epoch=-1)
        elif config['sched_version'] == 'cal_v1':
            self.lr_sched = optim.lr_scheduler.CosineAnnealingLR(self.optim,
                                                                 T_max=config['epochs'], eta_min=self.lr / 4,
                                                                 last_epoch=-1)
        elif config['sched_version'] == 'cawr_v0':
            self.lr_sched = optim.lr_scheduler.CosineAnnealingWarmRestarts(self.optim,
                                                                           T_0=10, T_mult=2, eta_min=self.lr / 2)
        elif config['sched_version'] == 'cawr_v1':
            self.lr_sched = optim.lr_scheduler.CosineAnnealingWarmRestarts(self.optim,
                                                                           T_0=25, T_mult=2, eta_min=self.lr / 4)
        else:
            self.lr_sched = None

    # Initialize
    def init_weights(self):
        self.param_count = 0
        for module in self.modules():
            if (isinstance(module, nn.Conv2d)
                    or isinstance(module, nn.Linear)
                    or isinstance(module, nn.Embedding)):
                if self.init == 'ortho':
                    init.orthogonal_(module.weight)
                elif self.init == 'N02':
                    init.normal_(module.weight, 0, 0.02)
                elif self.init in ['glorot', 'xavier']:
                    init.xavier_uniform_(module.weight)
                else:
                    print('Init style not recognized...')
                self.param_count += sum([p.data.nelement() for p in module.parameters()])
        print('Param count for G''s initialized parameters: %d' % self.param_count)

    def forward(self, z):
        z = z.view(z.size(0), z.size(1), 1, 1)
        out = self.l1(z)
        out = self.l2(out)
        out = self.l3(out)
        out, p1 = self.attn1(out)
        out = self.l4(out)
        out, p2 = self.attn2(out)
        out = self.last(out)

        return out, p1, p2


class Discriminator(nn.Module):
    """Discriminator, Auxiliary Classifier."""

    def __init__(self, config, batch_size=64, image_size=128, conv_dim=64, **kwargs):
        super(Discriminator, self).__init__()
        self.imsize = image_size
        layer1 = []
        layer2 = []
        layer3 = []
        last = []

        layer1.append(modules.SNTranspose2d(3, conv_dim, 4, 2, 1))
        layer1.append(nn.LeakyReLU(0.1))

        curr_dim = conv_dim

        layer2.append(modules.SNConv2d(curr_dim, curr_dim * 2, 4, 2, 1))
        layer2.append(nn.LeakyReLU(0.1))
        curr_dim = curr_dim * 2

        layer3.append(modules.SNConv2d(curr_dim, curr_dim * 2, 4, 2, 1))
        layer3.append(nn.LeakyReLU(0.1))
        curr_dim = curr_dim * 2

        if self.imsize == 64:
            layer4 = []
            layer4.append(modules.SNConv2d(curr_dim, curr_dim * 2, 4, 2, 1))
            layer4.append(nn.LeakyReLU(0.1))
            self.l4 = nn.Sequential(*layer4)
            curr_dim = curr_dim * 2
        self.l1 = nn.Sequential(*layer1)
        self.l2 = nn.Sequential(*layer2)
        self.l3 = nn.Sequential(*layer3)

        last.append(nn.Conv2d(curr_dim, 1, 4))
        self.last = nn.Sequential(*last)

        self.attn1 = Self_Attn(256, 'relu')
        self.attn2 = Self_Attn(512, 'relu')
        self.init = config['D_weight_init']
        if not config['skip_init']:
            self.init_weights()

        self.lr, self.B1, self.B2, self.adam_eps = config['D_lr'], config['D_beta1'], config['D_beta2'], config[
            'adam_eps']
        if config['d_mixed_precision']:
            print('Using fp16 adam in d...')
            import utils
            self.optim = utils.Adam16(params=self.parameters(), lr=self.lr,
                                      betas=(self.B1, self.B2), weight_decay=0,
                                      eps=self.adam_eps, amsgrad=kwargs['amsgrad'])
        else:
            self.optim = optim.Adam(params=self.parameters(), lr=self.lr,
                                    betas=(self.B1, self.B2), weight_decay=0,
                                    eps=self.adam_eps, amsgrad=kwargs['amsgrad'])

        if config['sched_version'] == 'default':
            self.lr_sched = None
        elif config['sched_version'] == 'cal_v0':
            self.lr_sched = optim.lr_scheduler.CosineAnnealingLR(self.optim,
                                                                 T_max=config['epochs'], eta_min=self.lr / 2,
                                                                 last_epoch=-1)
        elif config['sched_version'] == 'cal_v1':
            self.lr_sched = optim.lr_scheduler.CosineAnnealingLR(self.optim,
                                                                 T_max=config['epochs'], eta_min=self.lr / 4,
                                                                 last_epoch=-1)
        elif config['sched_version'] == 'cawr_v0':
            self.lr_sched = optim.lr_scheduler.CosineAnnealingWarmRestarts(self.optim,
                                                                           T_0=10, T_mult=2, eta_min=self.lr / 2)
        elif config['sched_version'] == 'cawr_v1':
            self.lr_sched = optim.lr_scheduler.CosineAnnealingWarmRestarts(self.optim,
                                                                           T_0=25, T_mult=2, eta_min=self.lr / 4)
        else:
            self.lr_sched = None

    # Initialize
    def init_weights(self):
        self.param_count = 0
        for module in self.modules():
            if (isinstance(module, nn.Conv2d)
                    or isinstance(module, nn.Linear)
                    or isinstance(module, nn.Embedding)):
                if self.init == 'ortho':
                    init.orthogonal_(module.weight)
                elif self.init == 'N02':
                    init.normal_(module.weight, 0, 0.02)
                elif self.init in ['glorot', 'xavier']:
                    init.xavier_uniform_(module.weight)
                else:
                    print('Init style not recognized...')
                self.param_count += sum([p.data.nelement() for p in module.parameters()])
        print('Param count for D''s initialized parameters: %d' % self.param_count)

    def forward(self, x):
        out = self.l1(x)
        out = self.l2(out)
        out = self.l3(out)
        out, p1 = self.attn1(out)
        out = self.l4(out)
        out, p2 = self.attn2(out)
        out = self.last(out)

        return out.squeeze(), p1, p2


# Parallelized G_D to minimize cross-gpu communication
# Without this, Generator outputs would get all-gathered and then rebroadcast.
class G_D(nn.Module):
    def __init__(self, G, D):
        super(G_D, self).__init__()
        self.G = G
        self.D = D

    def forward(self, z, gy, x=None, dy=None, train_G=False, return_G_z=False,
                split_D=False):
        # If training G, enable grad tape
        with torch.set_grad_enabled(train_G):
            G_z = self.G(z, self.G.shared(gy))
            # Cast as necessary
            if self.G.fp16 and not self.D.fp16:
                G_z = G_z.float()
            if self.D.fp16 and not self.G.fp16:
                G_z = G_z.half()
        # Split_D means to run D once with real data and once with fake,
        # rather than concatenating along the batch dimension.
        if split_D:
            D_fake, D_fake_features = self.D(G_z, gy)
            if x is not None:
                D_real, D_real_features = self.D(x, dy)
                return D_fake, D_fake_features, D_real, D_real_features
            else:
                if return_G_z:
                    return D_fake, D_fake_features, G_z
                else:
                    return D_fake, D_fake_features
        # If real data is provided, concatenate it with the Generator's output
        # along the batch dimension for improved efficiency.
        else:
            D_input = torch.cat([G_z, x], 0) if x is not None else G_z
            # if not self.D.use_dog_cnt and dy is not None and dy.dim()==2:
            #   D_class = torch.cat([gy, dy[:,0]], 0) if dy is not None else gy
            # else:
            D_class = torch.cat([gy, dy], 0) if dy is not None else gy
            # Get Discriminator output
            D_out, D_features = self.D(D_input, D_class)
            if x is not None:
                D_fake, D_real = torch.split(D_out, [G_z.shape[0], x.shape[0]])  # D_fake, D_real
                D_fake_features, D_real_features = torch.split(D_features, [G_z.shape[0], x.shape[
                    0]])  # D_fake_features, D_real_features
                return D_fake, D_fake_features, D_real, D_real_features

            else:
                if return_G_z:
                    return D_out, D_features, G_z
                else:
                    return D_out, D_features


if __name__ == '__main__':
    gen = Generator()
    print(gen)
