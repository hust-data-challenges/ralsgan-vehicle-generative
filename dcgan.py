import torch.nn as nn

import modules


class Generator(nn.Module):
    def __init__(self, config):
        super(Generator, self).__init__()
        self.config = config

        self.linear1 = nn.Linear(config['noise_dim'],
                                 config['img_size'] // config['scale_factor'] *
                                 config['img_size'] // config['scale_factor'] * 128)
        # 8, 8, 128
        self.transposed_conv2 = modules.TransposedConv(128, 512, 5, 1,
                                                       padding=2, dilation=1)  # 8, 8, 512
        self.dropout3 = nn.Dropout(config['dropout_rate'])
        self.transposed_conv4 = modules.TransposedConv(512, 256, 5, 2,
                                                       padding=2, dilation=1, output_padding=1)  # 16, 16, 256
        self.dropout5 = nn.Dropout(config['dropout_rate'])
        self.transposed_conv6 = modules.TransposedConv(256, 128, 5, 2,
                                                       padding=2, dilation=1, output_padding=1)  # 32, 32, 128
        self.transposed_conv7 = modules.TransposedConv(128, 64, 5, 2,
                                                       padding=2, dilation=1, output_padding=1)  # 64, 64, 64
        self.transposed_conv8 = modules.TransposedConv(64, 32, 5, 2,
                                                       padding=2, dilation=1, output_padding=1)  # 128, 128, 32
        self.linear8 = nn.Linear(32, 3)  # 128, 128, 3
        self.tanh9 = nn.Tanh()

    def forward(self, inputs):
        inputs = self.linear1(inputs)
        inputs = inputs.view(self.config['img_size'] // self.config['scale_factor'],
                             self.config['img_size'] // self.config['scale_factor'], 128)
        inputs = self.transposed_conv2(inputs)
        inputs = self.dropout3(inputs)
        inputs = self.transposed_conv4(inputs)
        inputs = self.dropout5(inputs)
        inputs = self.transposed_conv6(inputs)
        inputs = self.transposed_conv7(inputs)
        inputs = self.transposed_conv8(inputs)
        return self.tanh9(inputs)


class Discriminator(nn.Module):
    def __init__(self, config):
        super(Discriminator, self).__init__()
        if config['D_SpectralNorm']:
            self.convSN1 = nn.utils.spectral_norm(
                nn.Conv2d(3, 64, 5, 1, padding=2, dilation=1, bias=False))  # 128, 128, 64

            self.leaky2 = nn.LeakyReLU(config['leaky_slope'])  # 128, 128, 64
            self.conv3 = modules.ConvSNBlock(64, 64, 5, 2, padding=2, dilation=1,
                                             leaky_slope=config['leaky_slope'])  # 64, 64, 64
            self.conv4 = modules.ConvSNBlock(64, 128, 5, 2, padding=2, dilation=1,
                                             leaky_slope=config['leaky_slope'])  # 32, 32, 128
            self.conv5 = modules.ConvSNBlock(128, 256, 5, 2, padding=2, dilation=1,
                                             leaky_slope=config['leaky_slope'])  # 16, 16, 256
            # flatten
            self.linear6 = nn.Linear(65536, 1)
            self.sigmoid7 = nn.Sigmoid()
        else:
            self.convSN1 = nn.Conv2d(3, 64, 4, 2, padding=2, dilation=1, bias=False)  # 128, 128, 64
            self.leaky2 = nn.LeakyReLU(config['leaky_slope'])
            self.conv3 = modules.ConvBlock(64, 64, 5, 2, padding=2, dilation=1,
                                           leaky_slope=config['leaky_slope'])  # 64, 64, 64
            self.conv4 = modules.ConvBlock(64, 128, 5, 2, padding=2, dilation=1,
                                           leaky_slope=config['leaky_slope'])  # 32, 32, 128
            self.conv5 = modules.ConvBlock(128, 256, 5, 2, padding=2, dilation=1,
                                           leaky_slope=config['leaky_slope'])  # 16, 16, 256
            # flatten
            self.linear6 = nn.Linear(65536, 1)
            self.sigmoid7 = nn.Sigmoid()

    def forward(self, inputs):
        inputs = self.convSN1(inputs)
        inputs = self.leaky2(inputs)
        inputs = self.conv3(inputs)
        inputs = self.conv4(inputs)
        inputs = self.conv5(inputs)
        inputs = inputs.view(-1)
        inputs = self.linear6(inputs)
        return self.sigmoid7(inputs)
